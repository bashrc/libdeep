/*
libdeep - a library for deep learning
Copyright (C) 2013-2019  Bob Mottram <bob@freedombone.net>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided
with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include "libdeep/globals.h"
#include "libdeep/deeplearn.h"
#include "libdeep/deeplearn_images.h"
#include "libdeep/deeplearn_transform.h"

#define TITLE "Cell Nuclei Classification"

/* the dimensions of each face image */
int image_width = 64;
int image_height = 64;

deeptransform transnet;

deeplearn_conv convolution;
deeplearn learner;

/**
 * @brief Train the deep learner
 */
static void cellnuclei_training()
{
    int no_of_convolutions = 3;
    int no_of_deep_layers = 2;
    int max_features_per_convolution = 5*5;
    int final_image_width = 8;
    int feature_width = 5;
    float error_threshold[] = { 5.0, 5.0, 5.0 };
    unsigned int ctr, random_seed = 34217;
    float performance;
    unsigned int layer_itterations = 5000;
    int result;

    result = deeptransform_read_images("images", "classes",
                                       &transnet,
                                       image_width, image_height,
                                       no_of_convolutions,
                                       max_features_per_convolution,
                                       feature_width,
                                       final_image_width, final_image_width,
                                       layer_itterations,
                                       no_of_deep_layers,
                                       error_threshold,
                                       &random_seed);
    if (result != 0) {
        printf("result = %d\n", result);
        return;
    }

    printf("Number of images: %d\n", transnet.no_of_images);
    printf("Number of labeled training examples: %d\n",
           transnet.no_of_images*8/10);
    printf("Number of test examples: %d\n",transnet.no_of_images*2/10);

    if (transnet.no_of_images == 0)
        return;

    if (deeptransform_plot_training_image(&transnet,
                                          "test_input.png",
                                          640, 640, 0, 5) != 0) {
        return;
    }
    if (deeptransform_plot_training_image(&transnet,
                                          "test_output.png",
                                          640, 640, 1, 5) != 0) {
        return;
    }

    deeptransform_set_learning_rate(&transnet, 0.2f);

    deeptransform_set_dropouts(&transnet, 0.0f);

    transnet.history.interval = 800;

    deeplearn_set_title(&learner, TITLE);

    ctr = 99999;
    while (deeptransform_training(&transnet) == 0) {
        if (transnet.current_layer == 0) {
            if (ctr > 10000) {
                deeptransform_plot_features(&transnet, 0,
                                            "features0.png",
                                            640, 640);
                ctr = 0;
            }
            ctr++;
        }
    }

    deeptransform_plot_features(&transnet, 0,
                                "features0.png",
                                640, 640);

    if (deeptransform_training(&transnet) < 0) {
        printf("Training error\n");
        return;
    }

    printf("Training Completed\n");
    performance = deeptransform_get_performance(&transnet);
    if (performance >= 0) {
        printf("Test data set performance is %.2f%%\n",
               performance);
    }
    else {
        printf("Performance measurement error %d\n",
               (int)performance);
    }

    deeptransform_free(&transnet);
}

/**
 * @brief Main function
 */
int main(int argc, char* argv[])
{
    cellnuclei_training();
    return 0;
}
