/*
  libdeep - a library for deep learning
  Copyright (C) 2013-2018  Bob Mottram <bob@freedombone.net>

  deeptransform consists of two convolutional networks with
  a deep learner sandwiched in the middle. It is used to
  transform an input image into an output image, and is typically
  used for tasks such as segmentation

  |  \                             /  |
  |   \                           /   |
  |    \                         /    |
  |     \                       /     |
  |      \                     /      |
  |       \                   /       |
  |        \_________________/        |
  |                                   |
  |  input     Deep learner    output |
  |  conv   _________________  deconv |
  |        /                 \        |
  |       /                   \       |
  |      /                     \      |
  |     /                       \     |
  |    /                         \    |
  |   /                           \   |
  |  /                             \  |

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided
with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include "deeplearn_transform.h"

/**
 * @brief Initialise a deep transform
 * @param no_of_convolutions Number of layers in the convolution
 *        input and output stages
 * @param no_of_deep_layers Number of layers in the deep learner
 *        between the two convnets
 * @param image_width Horizontal resolution of input and output images
 * @param image_height Vertical resolution of input and output images
 * @param image_depth Depth of input and output images in bytes
 * @param max_features The number of features learned at each convolution layer
 * @param feature_width Width of each feature in the input image
 * @param final_image_width Width of the final stage of the input convnet
 *        and the initial layer of the output convnet
 * @param final_image_height Height of the final stage of the input convnet
 *        and the initial layer of the output convnet
 * @param layer_itterations The number of training itterations for each
 *        convolution layer
 * @param net Deep transform object
 * @param error_threshold Array containing learning thresholds (percent error)
 *        for each layer of both the convolution stage and the deep learner
 * @param random_seed Random number generator seed
 * @return zero on success
 */
int deeptransform_init(int no_of_convolutions,
                       int no_of_deep_layers,
                       int image_width,
                       int image_height,
                       int image_depth,
                       int max_features,
                       int feature_width,
                       int final_image_width,
                       int final_image_height,
                       unsigned int layer_itterations,
                       deeptransform * net,
                       float error_threshold[],
                       unsigned int * random_seed)
{
    int outputs_from_input_stage =
        final_image_width * final_image_height * image_depth;

    deeplearn_history_init(&net->history, "training.png",
                           "Training History",
                           "Time Step", "Training Error %");

    net->layer_itterations = layer_itterations;

    /* create input and output convolution stages */
    COUNTUP(conv_stage, 2) {
        net->convolution[conv_stage] =
            (deeplearn_conv*)malloc(sizeof(deeplearn_conv));

        if (!net->convolution[conv_stage])
            return -1;

        if (conv_init(no_of_convolutions,
                      image_width,
                      image_height,
                      image_depth,
                      max_features, feature_width,
                      final_image_width, final_image_height,
                      net->convolution[conv_stage]) != 0)
            return -2;
    }

    net->learner = (deeplearn*)malloc(sizeof(deeplearn));

    if (!net->learner)
        return -3;

    if (deeplearn_init(net->learner,
                       outputs_from_input_stage,
                       outputs_from_input_stage*8/10,
                       no_of_deep_layers,
                       outputs_from_input_stage,
                       &error_threshold[0],
                       random_seed) != 0)
        return -5;

    net->training_complete = 0;
    net->no_of_images = 0;
    net->source_images = NULL;
    net->output_images = NULL;

    /* default history settings */
    net->backprop_error = DEEPLEARN_UNKNOWN_ERROR;
    net->training_ctr = 0;

    net->current_layer = 0;
    net->training_set_index = NULL;
    net->test_set_index = NULL;
    return 0;
}

/**
 * @brief Is the given net still training?
 * @param net Deep transform object
 * @returns True if still training
 */
int deeptransform_is_training(deeptransform * net)
{
    return ((net->convolution[0]->current_layer <
             net->convolution[0]->no_of_layers) ||
            (net->convolution[1]->current_layer <
             net->convolution[1]->no_of_layers) ||
            (net->learner->training_complete != 0));
}

/**
 * @brief Frees memory
 * @param net Deep transform object
 */
void deeptransform_free(deeptransform * net)
{
    COUNTUP(i, 2) {
        conv_free(net->convolution[i]);
        free(net->convolution[i]);
    }

    deeplearn_free(net->learner);
    free(net->learner);

    if (net->no_of_images > 0) {
        COUNTDOWN(i, net->no_of_images) {
            if (net->source_images[i] != NULL) {
                free(net->source_images[i]);
                net->source_images[i] = 0;
            }
            if (net->output_images[i] != NULL) {
                free(net->output_images[i]);
                net->output_images[i] = 0;
            }
        }
        free(net->source_images);
        free(net->output_images);
        net->no_of_images = 0;
    }

    if (net->training_set_index != NULL)
        free(net->training_set_index);

    if (net->test_set_index != NULL)
        free(net->test_set_index);
}

/**
 * @brief Update the learning history
 * @param net Deep transform object
 */
static void deeptransform_update_history(deeptransform * net)
{
    float error_value;

    if (net->convolution[0]->current_layer <
        net->convolution[0]->no_of_layers)
        error_value = conv_get_error(net->convolution[0]);
    else {
        if (net->convolution[1]->current_layer <
            net->convolution[1]->no_of_layers)
            error_value = conv_get_error(net->convolution[1]);
        else
            error_value = net->learner->backprop_error;
    }

    if (error_value == DEEPLEARN_UNKNOWN_ERROR)
        error_value = 0;

    deeplearn_history_update(&net->history, error_value);
}

/**
 * @brief Saves the given deep net object to a file
 * @param fp File pointer
 * @param net Deep transform object
 * @return zero value on success
 */
int deeptransform_save(FILE * fp, deeptransform * net)
{
    if (conv_save(fp, net->convolution[0]) != 0)
        return -1;

    if (conv_save(fp, net->convolution[1]) != 0)
        return -2;

    if (deeplearn_save(fp, net->learner) != 0)
        return -3;

    return 0;
}

/**
 * @brief Loads a deep net object from file
 * @param fp File pointer
 * @param net Deep transform object
 * @return zero value on success
 */
int deeptransform_load(FILE * fp, deeptransform * net)
{
    if (conv_load(fp, net->convolution[0]) != 0)
        return -1;

    if (conv_load(fp, net->convolution[1]) != 0)
        return -2;

    if (deeplearn_load(fp, net->learner) != 0)
        return -3;

    return 0;
}

/**
 * @brief Sets the input values of the deep learner to the outputs of the
 *        input stage convolutional net.
 * @param learner Deep learner object
 * @param conv Input stage convolution object
 * @return zero on success
 */
static int deeptransform_set_learner_inputs(deeplearn * learner,
                                            deeplearn_conv * conv)
{
    if (learner->net->no_of_inputs != conv->no_of_outputs)
        return -1;

    COUNTDOWN(i, learner->net->no_of_inputs)
        deeplearn_set_input(learner, i, conv_get_output(conv, i));

    return 0;
}

/**
 * @brief Sets the input values of the output stage convolutional net
 * @param learner Deep learner object
 * @param conv Input stage convolution object
 * @return zero on success
 */
static int deeptransform_feedforward_learner_outputs(deeplearn * learner,
                                                     deeplearn_conv * conv)
{
    int top_layer = conv->no_of_layers-1;

    if (learner->net->no_of_outputs !=
        conv->layer[top_layer].width *
        conv->layer[top_layer].height *
        conv->layer[top_layer].depth)
        return -1;

    COUNTDOWN(i, learner->net->no_of_outputs)
        conv->layer[top_layer].layer[i] = deeplearn_get_output(learner, i);

    return 0;
}

/**
 * @brief Updates the current training error
 * @param net Deep transform object
 */
static void deeptransform_update_training_error(deeptransform * net)
{
    /* update the backprop error */
    if (net->current_layer <
        net->convolution[0]->no_of_layers)
        net->backprop_error = conv_get_error(net->convolution[0]);
    else {
        if (net->current_layer <
            net->convolution[0]->no_of_layers + net->convolution[1]->no_of_layers)
            net->backprop_error = conv_get_error(net->convolution[1]);
        else
            net->backprop_error = net->learner->backprop_error;
    }
}

/**
 * @brief Updates the index of the current layer being trained
 * @param net Deep transform object
 */
static void deeptransform_update_current_layer(deeptransform * net)
{
    /* set the current layer */
    net->current_layer =
        net->convolution[0]->current_layer + /* convolution */
        net->learner->current_hidden_layer + /* learner */
        net->convolution[1]->current_layer;  /* deconvolution */
}

/**
 * @brief Periodicaly plots the training error in a graph
 * @param net Deep transform object
 */
static void deeptransform_update_training_plot(deeptransform * net)
{
    /* plot a graph showing training progress */
    if (net->history.interval > 0) {
        if (net->training_ctr > net->history.interval) {
            if (strlen(net->history.filename) > 0)
                deeptransform_plot_history(net,
                                           DEEPLEARN_PLOT_WIDTH,
                                           DEEPLEARN_PLOT_HEIGHT);

            net->training_ctr = 0;
        }
        net->training_ctr++;
    }
}

/**
 * @brief Used during training to update error, current layer, etc
 * @param net Deep transform object
 */
static void deeptransform_update(deeptransform * net)
{
    deeptransform_update_training_error(net);
    deeptransform_update_current_layer(net);
    deeptransform_update_history(net);
    deeptransform_update_training_plot(net);
    net->convolution[0]->training = (net->learner->training_complete==0);
    net->convolution[1]->training = (net->learner->training_complete==0);
    net->training_complete = net->learner->training_complete;
}

/**
 * @brief Update routine for training the system
 * @param net Deep transform object
 * @param img_input Array containing input image
 * @param img_output Array containing output image
 * @param samples The number of samples to take from the image
 *        for convolution network training
 * @param layer_itterations The number of training itterations per layer
 *        for the convolution network
 * @return Zero on success
 */
int deeptransform_update_img(deeptransform * net,
                             unsigned char img_input[],
                             unsigned char img_output[],
                             int samples, unsigned int layer_itterations)
{
    unsigned int * random_seed = &net->learner->net->random_seed;
    int top_layer = net->convolution[1]->no_of_layers-1;

    if (net->convolution[0]->current_layer <
        net->convolution[0]->no_of_layers) {
        conv_learn(img_input, net->convolution[0], samples,
                   layer_itterations, random_seed);
        deeptransform_update(net);
        return 0;
    }

    if (net->convolution[1]->current_layer <
        net->convolution[1]->no_of_layers) {
        conv_learn(img_output, net->convolution[1], samples,
                   layer_itterations, random_seed);
        deeptransform_update(net);
        return 0;
    }

    conv_feed_forward(img_input, net->convolution[0],
                      net->convolution[0]->no_of_layers);

    if (deeptransform_set_learner_inputs(net->learner,
                                         net->convolution[0]) != 0)
        return -2;

    if (net->learner->training_complete == 0) {
        /* output image -> convolve */
        conv_feed_forward(img_output, net->convolution[1],
                          net->convolution[1]->no_of_layers);

        /* set the desired output values for the learner */
        COUNTDOWN(i, net->learner->net->no_of_outputs)
            deeplearn_set_output(net->learner, i, net->convolution[1]->layer[top_layer].layer[i]);

        /* learn convolution mapping at the highest level */
        deeplearn_update(net->learner);
        deeptransform_update(net);
    }
    else {
        deeplearn_feed_forward(net->learner);

        /* learner -> output stage top layer */
        if (deeptransform_feedforward_learner_outputs(net->learner,
                                                      net->convolution[1]) != 0)
            return -3;

        /* deconvolve to output stage first layer,
           which is the image */
        conv_feed_backwards(img_output, net->convolution[1],
                            net->convolution[1]->no_of_layers-1);
    }
    return 0;
}

/**
 * @brief Test a net with an unknown image
 * @param net Deep transform object
 * @param img Array containing input image
 * @return Zero on success
 */
int deeptransform_test_img(deeptransform * net,
                           unsigned char img_input[],
                           unsigned char img_output[])
{
    /* input image -> concolve */
    conv_feed_forward(img_input, net->convolution[0],
                      net->convolution[0]->no_of_layers);

    /* convolve output -> learner */
    if (deeptransform_set_learner_inputs(net->learner,
                                         net->convolution[0]) != 0)
        return -2;

    deeplearn_feed_forward(net->learner);

    /* learner -> output stage top layer */
    if (deeptransform_feedforward_learner_outputs(net->learner,
                                                  net->convolution[1]) != 0)
        return -3;

    /* deconvolve */
    conv_feed_backwards(img_output, net->convolution[1],
                        net->convolution[1]->no_of_layers-1);

    return 0;
}

/**
 * @brief Sets a desired output image
 * @param net Deep transform object
 * @param index Index of the output unit
 * @param image Image to set
 */
void deeptransform_set_output(deeptransform * net, int index,
                              unsigned char img[])
{
    COUNTDOWN(i,
              net->convolution[1]->layer[0].width*
              net->convolution[1]->layer[0].height*
              net->convolution[1]->layer[0].depth)
        net->convolution[1]->layer[0].layer[i] = (float)img[i]/255.0f;
}

/**
 * @brief Returns the output image
 * @param net Deep transform object
 * @param img Image to be output
 */
void deeptransform_get_output(deeptransform * net, unsigned char img[])
{
    COUNTDOWN(i,
              net->convolution[1]->layer[0].width*
              net->convolution[1]->layer[0].height*
              net->convolution[1]->layer[0].depth)
        img[i] = (unsigned char)(net->convolution[1]->layer[0].layer[i] * 255);
}

/**
 * @brief Sets the learning rate
 * @param net Deep transform object
 * @param rate the learning rate in the range 0.0 to 1.0
 */
void deeptransform_set_learning_rate(deeptransform * net, float rate)
{
    net->convolution[0]->learning_rate = rate;
    net->convolution[1]->learning_rate = rate;
    deeplearn_set_learning_rate(net->learner, rate);
}

/**
 * @brief Sets the percentage of units which drop out during training
 * @param net Deep transform object
 * @param dropout_percent Percentage of units which drop out in the range 0 to 100
 */
void deeptransform_set_dropouts(deeptransform * net, float dropout_percent)
{
    deeplearn_set_dropouts(net->learner, dropout_percent);
}

/**
 * @brief Uses gnuplot to plot the training error for the given learner
 * @param net Deep transform object
 * @param image_width Width of the image in pixels
 * @param image_height Height of the image in pixels
 * @return zero on success
 */
int deeptransform_plot_history(deeptransform * net,
                               int image_width, int image_height)
{
    return deeplearn_history_plot(&net->history,
                                  image_width, image_height);
}

/**
 * @brief Performs training
 * @param net Deep transform object
 * @returns zero on success
 */
int deeptransform_training(deeptransform * net)
{
    if (net->learner->training_complete != 0)
        return 1;

    if (net->no_of_images == 0)
        return 0;

    /* pick an image at random */
    int training_images = net->no_of_images*8/10;
    unsigned int * random_seed = &net->learner->net->random_seed;
    int index0 =
        rand_num(random_seed)%training_images;
    int index = net->training_set_index[index0];
    unsigned char * img_input = net->source_images[index];
    unsigned char * img_output = net->output_images[index];
    int samples = 20;

    if (deeptransform_update_img(net, img_input, img_output, samples,
                                 net->layer_itterations) != 0)
        return -2;

    return 0;
}

/**
 * @brief Returns performance on the test set
 * @param net Deep transform object
 * @return Percentage of correct pixel matches
 */
float deeptransform_get_performance(deeptransform * net)
{
    float error_percent;
    float total_error = 0;
    int test_images = net->no_of_images*2/10;
    int output_pixels =
        net->convolution[1]->layer[0].width*
        net->convolution[1]->layer[0].height*
        net->convolution[1]->layer[0].depth;

    if (net->no_of_images == 0)
        return -1;

    COUNTDOWN(i, test_images) {
        int index = net->test_set_index[i];

        if (deeptransform_test_img(net,
                                   net->source_images[index],
                                   net->output_images[index]) != 0) {
            printf("deeptransform_test_img failed\n");
            break;
        }

        COUNTDOWN(i, output_pixels) {
            error_percent =
                net->output_images[index][i] -
                (unsigned char)(net->convolution[1]->layer[0].layer[i] * 255);

            total_error += error_percent*error_percent;
        }
    }

    return 100 -
        ((float)sqrt(total_error / (output_pixels*test_images)) * 100);
}

/**
 * @brief Creates training and test arrays which contain randomly ordered
 *        indexes to the main images array. This tries to ensure that there
 *        is no bias depending on the sequences of inputs during training.
 * @param net Deep transform object
 */
int deeptransform_create_training_test_sets(deeptransform * net)
{
    int i, j;

    /* create arrays to store randomly ordered array indexes
       for training and test sets */
    int training_images = net->no_of_images*8/10;

    INTALLOC(net->training_set_index, training_images+1);
    if (!net->training_set_index)
        return -1;

    i = 0;
    while (i < training_images) {
        int index =
            rand_num(&net->learner->net->random_seed)%net->no_of_images;

        for (j = 0; j < i; j++) {
            if (net->training_set_index[j] == index)
                break;
        }

        if (j == i)
            net->training_set_index[i++] = index;
    }
    int test_images = 0;

    INTALLOC(net->test_set_index,
             net->no_of_images - training_images + 1);
    if (!net->test_set_index) return -2;

    for (i = 0; i < net->no_of_images; i++) {
        for (j = 0; j < training_images; j++) {
            if (net->training_set_index[j] == i)
                break;
        }

        if (j == training_images)
            net->test_set_index[test_images++] = i;
    }
    return 0;
}

/**
 * @brief Reads images from a given directory and creates a deep transform
 * @param source_directory Directory containing png source images
 * @param output_directory Directory containing png output images
 * @param net Deep transform object
 * @param image_width Image width
 * @param image_height Image height
 * @param no_of_convolutions The number of convolution layers
 * @param max_features_per_convolution Number of features learned at
 *        each convolution layer
 * @param feature_width Width of features in the input image
 * @param final_image_width Width of the output of the convolutional network
 * @param final_image_height Height of the output of the convolutional network
 * @param layer_itterations Number of training itterations for each
 *        convolution layer
 * @param no_of_deep_layers Number of layers for the deep learner
 * @param error_threshold Training error thresholds for each hidden layer
 * @param random_seed Random number seed
 * @return zero on success
 */
int deeptransform_read_images(char * source_directory,
                              char * output_directory,
                              deeptransform * net,
                              int image_width, int image_height,
                              int no_of_convolutions,
                              int max_features_per_convolution,
                              int feature_width,
                              int final_image_width,
                              int final_image_height,
                              unsigned int layer_itterations,
                              int no_of_deep_layers,
                              float error_threshold[],
                              unsigned int * random_seed)
{
    int prev_no_of_images;

    if (deeptransform_init(no_of_convolutions,
                           no_of_deep_layers,
                           image_width, image_height, 1,
                           max_features_per_convolution,
                           feature_width,
                           final_image_width,
                           final_image_height,
                           layer_itterations,
                           net,
                           error_threshold,
                           random_seed) != 0)
        return -1;

    net->no_of_images =
        load_training_images(source_directory,
                             &net->source_images,
                             image_width, image_height);
    if (net->no_of_images <= 0)
        return -2;

    prev_no_of_images = net->no_of_images;

    net->no_of_images =
        load_training_images(output_directory,
                             &net->output_images,
                             image_width, image_height);
    if (net->no_of_images <= 0)
        return -3;

    if (net->no_of_images != prev_no_of_images) {
        printf("Number of images are different, source=%d outputs=%d\n",
               prev_no_of_images, net->no_of_images);
        return -4;
    }

    if (deeptransform_create_training_test_sets(net) != 0)
        return -5;

    return 0;
}

/**
 * @brief Plots the features learned for a given convolution layer
 * @param net Deep transform object
 * @param layer_index Index number of the convolution layer
 * @param filename Filename to save the image as (must be PNG format)
 * @param img_width Image width
 * @param img_height Image height
 * @return zero on success
 */
int deeptransform_plot_features(deeptransform * net,
                                int layer_index,
                                char * filename,
                                int img_width, int img_height)
{
    unsigned char * img;

    /* allocate memory for the image */
    UCHARALLOC(img, img_width*img_height*3);
    if (!img)
        return -1;

    int retval = conv_draw_features(img, img_width, img_height, 3,
                                    layer_index, net->convolution[0]);
    if (retval != 0)
        return retval;

    deeplearn_write_png_file(filename,
                             (unsigned int)img_width,
                             (unsigned int)img_height,
                             24, img);

    free(img);
    return 0;
}

/**
 * @brief Saves the source or output layer as an image
 * @param net Deep transform object
 * @param filename Filename to save the image as (must be PNG format)
 * @param img_width Image width
 * @param img_height Image height
 * @param image_type 0=source image 1=output image
 * @return zero on success
 */
int deeptransform_plot_image(deeptransform * net,
                             char * filename,
                             int img_width, int img_height,
                             int image_type)
{
    unsigned char * img;
    int depth = net->convolution[image_type]->layer[0].depth;
    int layer_width = net->convolution[image_type]->layer[0].width;
    int layer_height = net->convolution[image_type]->layer[0].height;
    float * layer = net->convolution[image_type]->layer[0].layer;
    int xx, yy, n1, n2;

    /* allocate memory for the image */
    UCHARALLOC(img, img_width*img_height*3);
    if (!img)
        return -1;

    COUNTDOWN(y, img_height) {
        yy = y * layer_height / img_height;
        COUNTDOWN(x, img_width) {
            xx = x * layer_width / img_width;
            n1 = (yy * layer_width + xx) * depth;
            n2 = (y * img_width + x)*3;
            COUNTUP(c, 3) {
                img[n2+c] = (unsigned char)(layer[n1] * 255);
                if (depth == 3) n1++;
            }
        }
    }

    deeplearn_write_png_file(filename,
                             (unsigned int)img_width,
                             (unsigned int)img_height,
                             24, img);
    free(img);
    return 0;
}

/**
 * @brief Saves an image from the source or output images
 *        This can be used to check that the training set
 *        has been loaded into memory
 * @param net Deep transform object
 * @param filename Filename to save the image as (must be PNG format)
 * @param img_width Image width
 * @param img_height Image height
 * @param image_type 0=input image 1=output image
 * @param image_index Index within the training set
 * @return zero on success
 */
int deeptransform_plot_training_image(deeptransform * net,
                                      char * filename,
                                      int img_width, int img_height,
                                      int image_type,
                                      int image_index)
{
    unsigned char * img;
    int depth = net->convolution[image_type]->layer[0].depth;
    int layer_width = net->convolution[image_type]->layer[0].width;
    int layer_height = net->convolution[image_type]->layer[0].height;
    unsigned char * set_img =
        net->source_images[image_index % net->no_of_images];
    int xx, yy, n1, n2;

    if (image_type != 0)
        set_img = net->output_images[image_index % net->no_of_images];

    /* allocate memory for the image */
    UCHARALLOC(img, img_width*img_height*3);
    if (!img)
        return -1;

    COUNTDOWN(y, img_height) {
        yy = y * layer_height / img_height;
        COUNTDOWN(x, img_width) {
            xx = x * layer_width / img_width;
            n1 = (yy * layer_width + xx) * depth;
            n2 = (y * img_width + x)*3;
            COUNTUP(c, 3) {
                img[n2+c] = set_img[n1];
                if (depth == 3) n1++;
            }
        }
    }

    deeplearn_write_png_file(filename,
                             (unsigned int)img_width,
                             (unsigned int)img_height,
                             24, img);
    free(img);
    return 0;
}
