/*
  libdeep - a library for deep learning
  Copyright (C) 2013-2018  Bob Mottram <bob@freedombone.net>

  deeptransform consists of two convolutional networks with
  a deep learner sandwiched in the middle. It is used to
  transform an input image into an output image, and is typically
  used for tasks such as segmentation

  |  \                             /  |
  |   \                           /   |
  |    \                         /    |
  |     \                       /     |
  |      \                     /      |
  |       \                   /       |
  |        \_________________/        |
  |                                   |
  |  input     Deep learner    output |
  |  conv   _________________  deconv |
  |        /                 \        |
  |       /                   \       |
  |      /                     \      |
  |     /                       \     |
  |    /                         \    |
  |   /                           \   |
  |  /                             \  |

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided
with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DEEPLEARN_TRANSFORM_H
#define DEEPLEARN_TRANSFORM_H

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <dirent.h>
#include <omp.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <stdarg.h>

#include "globals.h"
#include "deeplearn_random.h"
#include "backprop_neuron.h"
#include "encoding.h"
#include "backprop.h"
#include "deeplearn.h"
#include "deeplearn_features.h"
#include "deeplearn_conv.h"
#include "lodepng.h"
#include "utils.h"

typedef struct {
    /* convolution layers */
    deeplearn_conv *convolution[2];

    /* deep learning layers between the two convnets */
    deeplearn * learner;

    int training_complete;

    /* current layer being trained */
    int current_layer;

    /* number of training itterations per convolution layer */
    unsigned int layer_itterations;

    /* training/test images */
    int no_of_images;
    unsigned char ** source_images;
    unsigned char ** output_images;

    unsigned int training_ctr;

    /* current backprop error */
    float backprop_error;

    deeplearn_history history;

    /* array index numbers for training and test set */
    int * training_set_index;
    int * test_set_index;
} deeptransform;

int deeptransform_init(int no_of_convolutions,
                       int no_of_deep_layers,
                       int image_width,
                       int image_height,
                       int image_depth,
                       int max_features,
                       int feature_width,
                       int final_image_width,
                       int final_image_height,
                       unsigned int layer_itterations,
                       deeptransform * net,
                       float error_threshold[],
                       unsigned int * random_seed);
void deeptransform_free(deeptransform * net);
int deeptransform_save(FILE * fp, deeptransform * net);
int deeptransform_load(FILE * fp, deeptransform * net);
int deeptransform_update_img(deeptransform * net,
                             unsigned char img_input[],
                             unsigned char img_output[],
                             int samples, unsigned int layer_itterations);
int deeptransform_test_img(deeptransform * net,
                           unsigned char img_input[],
                           unsigned char img_output[]);
void deeptransform_set_learning_rate(deeptransform * net, float rate);
void deeptransform_set_dropouts(deeptransform * net, float dropout_percent);
int deeptransform_read_images(char * source_directory,
                              char * output_directory,
                              deeptransform * net,
                              int image_width, int image_height,
                              int no_of_convolutions,
                              int max_features_per_convolution,
                              int feature_width,
                              int final_image_width,
                              int final_image_height,
                              unsigned int layer_itterations,
                              int no_of_deep_layers,
                              float error_threshold[],
                              unsigned int * random_seed);
int deeptransform_training(deeptransform * net);
int deeptransform_plot_history(deeptransform * net,
                               int image_width, int image_height);
float deeptransform_get_performance(deeptransform * net);
void deeptransform_set_output(deeptransform * net, int index,
                              unsigned char img[]);
void deeptransform_get_output(deeptransform * net, unsigned char img[]);
int deeptransform_plot_features(deeptransform * net,
                                int layer_index,
                                char * filename,
                                int img_width, int img_height);
void deeptransform_set_learning_rate(deeptransform * net, float rate);
int deeptransform_is_training(deeptransform * net);
int deeptransform_plot_image(deeptransform * net,
                             char * filename,
                             int img_width, int img_height,
                             int image_type);
int deeptransform_plot_training_image(deeptransform * net,
                                      char * filename,
                                      int img_width, int img_height,
                                      int image_type,
                                      int image_index);

#endif
